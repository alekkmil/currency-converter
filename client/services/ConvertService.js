export async function convertCurrency(body) {
    const data = await fetch('http://localhost:3000/api/convertCurrency', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    })
    return await data.json();
}