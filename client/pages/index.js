import Head from 'next/head'
import ConverterCalculator from '../components/CurrencyConverter'
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from '../styles/Home.module.css'
import {SSRProvider} from '@react-aria/ssr'

export default function Home() {
  return (
    <SSRProvider>
      <div className={styles.container}>
        <Head>
          <title>Currency Calculator</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <h1>Currency Calculator</h1>
        <ConverterCalculator />
      </div>
    </SSRProvider>

  )
}
