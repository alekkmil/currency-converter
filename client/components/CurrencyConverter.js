import CurrencyRow from "./CurrencyRow"
import React, { useEffect, useState } from 'react';
import { convertCurrency } from "../services/ConvertService";

function ConverterCalculator() {
    // should probably be get from a config or smth not hardcoded
    const currencyOptions = ['EURO', 'BGN', 'USD', 'GBP', 'RUB', 'CAD'];

    const [fromCurrency, setFromCurrency] = useState(currencyOptions[0]);
    const [toCurrency, setToCurrency] = useState(currencyOptions[1]);
    const [fromAmount, setFromAmount] = useState(1);
    const [toAmount, setToAmount] = useState(1);
    const [amountInFromCurrency, setAmountInFromCurrency] = useState(true);

    useEffect(() => {
        if (fromCurrency != null && toCurrency != null) {
            const opts = amountInFromCurrency ? { from: fromCurrency, to: toCurrency, amount: fromAmount } :
                { from: toCurrency, to: fromCurrency, amount: toAmount };
            convertCurrency(opts)
                .then(data => {
                    amountInFromCurrency ? setToAmount(data[toCurrency]) : setFromAmount(data[fromCurrency]);
                });
        }
    }, [amountInFromCurrency, fromAmount, fromCurrency, toAmount, toCurrency]);

    function handleFromAmountChange(e) {
        setFromAmount(+e.target.value);
        setAmountInFromCurrency(true);
    }

    function handleToAmountChange(e) {
        setToAmount(+e.target.value);
        setAmountInFromCurrency(false);
    }

    return (
        <div>
            <h1>Convert</h1>
            <CurrencyRow
                currencyOptions={currencyOptions}
                selectedCurrency={fromCurrency}
                onChangeCurrency={e => setFromCurrency(e.target.value)}
                onChangeAmount={handleFromAmountChange}
                amount={fromAmount}
            />
            <div className="equals">=</div>
            <CurrencyRow
                currencyOptions={currencyOptions}
                selectedCurrency={toCurrency}
                onChangeCurrency={e => setToCurrency(e.target.value)}
                onChangeAmount={handleToAmountChange}
                amount={toAmount}
            />
        </div>
    )
}

export default ConverterCalculator;