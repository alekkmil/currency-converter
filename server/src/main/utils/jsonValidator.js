const Ajv = require('ajv');
const ajv = new Ajv();

const validate = (data, schema) => {
    const valid = ajv.validate(schema, data)
    if (!valid) {
        const errMsg = transformError(ajv.errors);
        console.error(errMsg);
        throw new Error(errMsg);
    }
}

const transformError = (errors) => {
    return errors.map(err => `${err.instancePath} ${err.message}`).join(', ');
}

module.exports = {
    jsonValidator: validate
}