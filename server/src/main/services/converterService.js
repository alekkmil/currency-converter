const currencies = require('../../resourses/data/currencies.json');

const convertCurrency = ({ from, to, amount }) => {
    const baseUnit = process.env.BASEUNIT;

    if (from === to) {
        return { [to]: amount.toFixed(2) };
    }

    if (from !== baseUnit) {
        const amountInEuro = amount / Number(currencies[baseUnit][from]);
        return to === baseUnit ? {
            [to]: amountInEuro.toFixed(2)
        } : {
            [to]: (amountInEuro * Number(currencies[baseUnit][to])).toFixed(2)
        };
    }

    return {
        [to]: (Number(currencies[baseUnit][to]) * amount).toFixed(2)
    };
}

module.exports = {
    convertCurrency
}