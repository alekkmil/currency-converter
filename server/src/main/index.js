const express = require('express');
const { converterRouter } = require('./routes/converterRouter');
const cors = require('cors');

require(`dotenv`).config();

const main = () => {
    const app = express();

    const port = Number(process.env.PORT) || 3000;

    app.use(cors());
    app.use(express.json());
    app.use('/api/convertCurrency', converterRouter);

    app.listen(port);

    console.log(`App listening on port ${port}`);
}

try {
    main();
} catch (e) {
    console.error('Error during main().', e);
    process.exit(1);
}