const router = require('express').Router();
const { convertCurrency } = require('../services/converterService');
const { jsonValidator } = require('../utils/jsonValidator');
const schema = require('../../resourses/schemas/currencyConvertSchema.json');

router.post('/', (req, res) => {
    const data = req.body;

    try {
        jsonValidator(data, schema);
    } catch (e) {
        return res.status(400).send({
            status: 400,
            erorr: 'Bad Request',
            path: req.baseUrl,
            message: e.message,
        });
    }

    const convertedCurr = convertCurrency(data);

    return res.status(200).send(convertedCurr);
});

module.exports = {
    converterRouter: router,
}